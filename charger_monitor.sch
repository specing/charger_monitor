EESchema Schematic File Version 4
LIBS:charger_monitor-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Charger monitor"
Date ""
Rev ""
Comp ""
Comment1 "License: CC BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/"
Comment2 "Author: Fedja Beader"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L f072rb:stm32f072-disco A1
U 1 1 5D712D09
P 5950 3100
F 0 "A1" H 5950 4925 50  0000 C CNN
F 1 "stm32f072-disco" H 5950 4834 50  0000 C CNN
F 2 "stm32f072disco:stm32f072disco" H 7650 2650 50  0001 C CNN
F 3 "" H 7650 2650 50  0001 C CNN
	1    5950 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5D722A2F
P 7350 1500
F 0 "J3" H 7322 1432 50  0000 R CNN
F 1 "relay pwr" H 7322 1523 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Horizontal" H 7350 1500 50  0001 C CNN
F 3 "~" H 7350 1500 50  0001 C CNN
	1    7350 1500
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D75C03E
P 2550 1950
F 0 "#PWR0102" H 2550 1700 50  0001 C CNN
F 1 "GND" H 2555 1777 50  0000 C CNN
F 2 "" H 2550 1950 50  0001 C CNN
F 3 "" H 2550 1950 50  0001 C CNN
	1    2550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1950 2550 1900
$Comp
L power:GND #PWR0104
U 1 1 5D79437C
P 2550 3350
F 0 "#PWR0104" H 2550 3100 50  0001 C CNN
F 1 "GND" H 2555 3177 50  0000 C CNN
F 2 "" H 2550 3350 50  0001 C CNN
F 3 "" H 2550 3350 50  0001 C CNN
	1    2550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3350 2550 3300
NoConn ~ 5050 1600
NoConn ~ 5050 1700
NoConn ~ 6800 2500
NoConn ~ 5050 2000
NoConn ~ 5050 2100
NoConn ~ 5050 2200
NoConn ~ 5050 2400
NoConn ~ 5050 2500
NoConn ~ 5050 2600
NoConn ~ 5050 2700
NoConn ~ 5050 2900
NoConn ~ 5050 3000
NoConn ~ 5050 3100
NoConn ~ 5050 3200
NoConn ~ 5050 3300
NoConn ~ 5050 3400
NoConn ~ 5050 3500
NoConn ~ 5050 3600
NoConn ~ 5050 3700
NoConn ~ 5050 3800
NoConn ~ 5050 3900
NoConn ~ 5050 4000
NoConn ~ 5050 4100
NoConn ~ 5050 4200
NoConn ~ 5050 4300
NoConn ~ 5050 4400
NoConn ~ 5050 4500
NoConn ~ 5050 4600
NoConn ~ 6800 4700
NoConn ~ 6800 4600
NoConn ~ 6800 4000
NoConn ~ 6800 3900
NoConn ~ 6800 4200
NoConn ~ 6800 4100
NoConn ~ 6800 3800
NoConn ~ 6800 3700
NoConn ~ 6800 3600
NoConn ~ 6800 3500
NoConn ~ 6800 3400
NoConn ~ 6800 3300
NoConn ~ 6800 3200
NoConn ~ 6800 3100
NoConn ~ 6800 3000
NoConn ~ 6800 2700
NoConn ~ 6800 2600
NoConn ~ 6800 2400
NoConn ~ 6800 2300
NoConn ~ 6800 2200
NoConn ~ 6800 2100
NoConn ~ 6800 2000
NoConn ~ 6800 1900
NoConn ~ 6800 1800
NoConn ~ 6800 1700
NoConn ~ 6800 4300
NoConn ~ 6800 4400
NoConn ~ 6800 4500
Wire Wire Line
	6800 1500 7150 1500
$Comp
L Connector:Conn_01x01_Male J8
U 1 1 5D888F00
P 1850 2950
F 0 "J8" V 1912 2994 50  0000 L CNN
F 1 "Batt-" V 1800 2850 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1850 2950 50  0001 C CNN
F 3 "~" H 1850 2950 50  0001 C CNN
	1    1850 2950
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J7
U 1 1 5D88ECC0
P 1850 2750
F 0 "J7" V 1912 2794 50  0000 L CNN
F 1 "Batt+" V 1800 2650 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1850 2750 50  0001 C CNN
F 3 "~" H 1850 2750 50  0001 C CNN
	1    1850 2750
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5D89F412
P 1300 1100
F 0 "J1" V 1362 1144 50  0000 L CNN
F 1 "charger+" V 1250 900 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1300 1100 50  0001 C CNN
F 3 "~" H 1300 1100 50  0001 C CNN
	1    1300 1100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5D8A023A
P 1300 1300
F 0 "J2" V 1362 1344 50  0000 L CNN
F 1 "charger-" V 1250 1150 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1300 1300 50  0001 C CNN
F 3 "~" H 1300 1300 50  0001 C CNN
	1    1300 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 1500 1300 1650
$Comp
L power:GND #PWR0103
U 1 1 5D75F3EB
P 1300 1650
F 0 "#PWR0103" H 1300 1400 50  0001 C CNN
F 1 "GND" H 1305 1477 50  0000 C CNN
F 2 "" H 1300 1650 50  0001 C CNN
F 3 "" H 1300 1650 50  0001 C CNN
	1    1300 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5D8B197E
P 1850 1100
F 0 "J5" V 1912 1144 50  0000 L CNN
F 1 "relay0-NO" V 1800 900 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1850 1100 50  0001 C CNN
F 3 "~" H 1850 1100 50  0001 C CNN
	1    1850 1100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J6
U 1 1 5D8B23A4
P 1850 1300
F 0 "J6" V 1912 1344 50  0000 L CNN
F 1 "relay0-comm" V 1800 1100 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1850 1300 50  0001 C CNN
F 3 "~" H 1850 1300 50  0001 C CNN
	1    1850 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 900  2550 900 
Wire Wire Line
	1850 900  1300 900 
Connection ~ 1850 900 
Wire Wire Line
	1850 1500 1850 2300
Wire Wire Line
	1850 2300 2550 2300
Connection ~ 1850 2300
Wire Wire Line
	1850 2300 1850 2550
Text Notes 5600 1200 0    50   ~ 0
Have: a 4* 5V-supply\nisolated active-low\n songle relay module\nthat activates at 3v already
NoConn ~ 5050 4700
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 5D94A035
P 7350 2900
F 0 "J9" H 7322 2782 50  0000 R CNN
F 1 "conn_UART" H 7322 2873 50  0000 R CNN
F 2 "" H 7350 2900 50  0001 C CNN
F 3 "~" H 7350 2900 50  0001 C CNN
	1    7350 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 2800 7150 2800
Wire Wire Line
	7150 2900 6800 2900
$Comp
L Connector:Conn_01x01_Male J10
U 1 1 5D94C647
P 7350 3000
F 0 "J10" H 7322 2932 50  0000 R CNN
F 1 "Conn_UART_GND" H 7322 3023 50  0000 R CNN
F 2 "" H 7350 3000 50  0001 C CNN
F 3 "~" H 7350 3000 50  0001 C CNN
	1    7350 3000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D94DF4E
P 7150 3100
F 0 "#PWR0106" H 7150 2850 50  0001 C CNN
F 1 "GND" H 7155 2927 50  0000 C CNN
F 2 "" H 7150 3100 50  0001 C CNN
F 3 "" H 7150 3100 50  0001 C CNN
	1    7150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3000 7150 3100
Text Notes 7200 2700 0    50   ~ 0
115200 bps, no parity
NoConn ~ 4800 2200
NoConn ~ 4800 2100
NoConn ~ 4800 2000
$Comp
L Connector:Conn_01x06_Male J4
U 1 1 5D71A0C7
P 4600 2100
F 0 "J4" H 4572 1937 50  0000 R CNN
F 1 "relays" H 4572 2028 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 4600 2100 50  0001 C CNN
F 3 "~" H 4600 2100 50  0001 C CNN
F 4 "GND" H 4750 2350 50  0000 R CNN "gndmark"
	1    4600 2100
	1    0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5D9AE8D2
P 2550 1400
F 0 "R2" H 2620 1446 50  0000 L CNN
F 1 "3k3" H 2620 1355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 1400 50  0001 C CNN
F 3 "~" H 2550 1400 50  0001 C CNN
	1    2550 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5D9AEF99
P 2550 1750
F 0 "R3" H 2620 1796 50  0000 L CNN
F 1 "3k3" H 2620 1705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 1750 50  0001 C CNN
F 3 "~" H 2550 1750 50  0001 C CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D9B17E7
P 2550 1050
F 0 "R1" H 2620 1096 50  0000 L CNN
F 1 "100K" H 2620 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 1050 50  0001 C CNN
F 3 "~" H 2550 1050 50  0001 C CNN
	1    2550 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1200 2550 1250
Wire Wire Line
	2550 1550 2550 1600
Wire Wire Line
	2550 2950 2550 3000
Wire Wire Line
	2550 2600 2550 2650
$Comp
L Device:R R4
U 1 1 5D9B0FE8
P 2550 2450
F 0 "R4" H 2620 2496 50  0000 L CNN
F 1 "100K" H 2620 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 2450 50  0001 C CNN
F 3 "~" H 2550 2450 50  0001 C CNN
	1    2550 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5D9AC5DC
P 2550 3150
F 0 "R6" H 2620 3196 50  0000 L CNN
F 1 "3k3" H 2620 3105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 3150 50  0001 C CNN
F 3 "~" H 2550 3150 50  0001 C CNN
	1    2550 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D9AB23E
P 2550 2800
F 0 "R5" H 2620 2846 50  0000 L CNN
F 1 "3k3" H 2620 2755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2480 2800 50  0001 C CNN
F 3 "~" H 2550 2800 50  0001 C CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
Text Label 3500 2800 0    50   ~ 0
battery_volt
Wire Wire Line
	1850 3300 1850 3150
Connection ~ 2550 3300
Wire Wire Line
	2550 2650 2950 2650
Connection ~ 2550 2650
Text Label 4250 1250 0    50   ~ 0
chg1_present
Connection ~ 2550 1250
Wire Wire Line
	5050 2300 4800 2300
Wire Wire Line
	4800 1900 5050 1900
Wire Wire Line
	4800 1800 4800 1500
Wire Wire Line
	4800 1500 5050 1500
Wire Wire Line
	4900 1250 4900 1800
Wire Wire Line
	4900 1800 5050 1800
Wire Wire Line
	2550 1250 4900 1250
$Comp
L Isolator:NSL-32 U1
U 1 1 5D9F4694
P 1400 6900
F 0 "U1" H 1400 7217 50  0000 C CNN
F 1 "FL 817C F631" H 1400 7126 50  0000 C CNN
F 2 "" H 1400 6600 50  0001 C CNN
F 3 "" H 1450 6900 50  0001 C CNN
	1    1400 6900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5D9F51E4
P 700 6800
F 0 "#PWR01" H 700 6650 50  0001 C CNN
F 1 "VCC" H 717 6973 50  0000 C CNN
F 2 "" H 700 6800 50  0001 C CNN
F 3 "" H 700 6800 50  0001 C CNN
	1    700  6800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D9F5BAF
P 900 6800
F 0 "R7" V 693 6800 50  0000 C CNN
F 1 "1k" V 784 6800 50  0000 C CNN
F 2 "" V 830 6800 50  0001 C CNN
F 3 "~" H 900 6800 50  0001 C CNN
	1    900  6800
	0    1    1    0   
$EndComp
Wire Wire Line
	750  6800 700  6800
Wire Wire Line
	1100 6800 1050 6800
$Comp
L Device:LED D1
U 1 1 5D9FBB25
P 850 7150
F 0 "D1" V 889 7033 50  0000 R CNN
F 1 "LED" V 798 7033 50  0000 R CNN
F 2 "" H 850 7150 50  0001 C CNN
F 3 "~" H 850 7150 50  0001 C CNN
	1    850  7150
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J11
U 1 1 5D9FD349
P 850 7600
F 0 "J11" V 1004 7512 50  0000 R CNN
F 1 "signal" V 913 7512 50  0000 R CNN
F 2 "" H 850 7600 50  0001 C CNN
F 3 "~" H 850 7600 50  0001 C CNN
	1    850  7600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	850  7300 850  7400
Wire Wire Line
	850  7000 1100 7000
$Comp
L power:+5V #PWR02
U 1 1 5D9FFBDF
P 1800 6800
F 0 "#PWR02" H 1800 6650 50  0001 C CNN
F 1 "+5V" H 1815 6973 50  0000 C CNN
F 2 "" H 1800 6800 50  0001 C CNN
F 3 "" H 1800 6800 50  0001 C CNN
	1    1800 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6800 1700 6800
$Comp
L Device:R R8
U 1 1 5DA045A4
P 1900 7000
F 0 "R8" V 1693 7000 50  0000 C CNN
F 1 "1k" V 1784 7000 50  0000 C CNN
F 2 "" V 1830 7000 50  0001 C CNN
F 3 "~" H 1900 7000 50  0001 C CNN
	1    1900 7000
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 7000 1700 7000
Wire Wire Line
	2100 7000 2050 7000
NoConn ~ 2100 7000
Text Notes 1700 7150 0    50   ~ 0
to transistor
Text Notes 900  7650 0    50   ~ 0
mcu input
Text Notes 700  6500 0    50   ~ 0
Relay module mcu-facing circuit
Wire Notes Line
	600  6400 600  7700
Wire Notes Line
	600  7700 2200 7700
Wire Notes Line
	2200 7700 2200 6400
Wire Notes Line
	2200 6400 600  6400
$Comp
L Device:C_Small C1
U 1 1 5DA1EAAE
P 2950 3100
F 0 "C1" H 3042 3146 50  0000 L CNN
F 1 "100nF" H 3042 3055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2950 3100 50  0001 C CNN
F 3 "~" H 2950 3100 50  0001 C CNN
F 4 "0.5LSB err: at least 66nF for 8pF C_s" H 3700 2950 50  0000 C CNN "Comment"
	1    2950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2800 2950 3000
Wire Wire Line
	2950 3200 2950 3300
Wire Wire Line
	2950 3300 2550 3300
Wire Wire Line
	2950 2650 2950 2800
Connection ~ 2950 2800
Wire Wire Line
	2950 2800 5050 2800
$Comp
L power:GND #PWR0105
U 1 1 5DA2EF5B
P 4800 2500
F 0 "#PWR0105" H 4800 2250 50  0001 C CNN
F 1 "GND" H 4805 2327 50  0000 C CNN
F 2 "" H 4800 2500 50  0001 C CNN
F 3 "" H 4800 2500 50  0001 C CNN
	1    4800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2300 4800 2500
Connection ~ 4800 2300
Wire Wire Line
	1850 3300 2550 3300
NoConn ~ 6800 1600
$EndSCHEMATC
