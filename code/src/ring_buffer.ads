--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

package Ring_Buffer is
   type RingBuffer is limited private;
   procedure Initialize(RB: in out RingBuffer);
   procedure Put(RB: in out RingBuffer; C : in Character);
   function Get(RB: in out RingBuffer) return Character;
   function Is_Empty(RB: in RingBuffer) return Boolean;
   function Is_Full(RB: in RingBuffer) return Boolean;
private
   size : constant := 256;
   type Index is mod size;
   type Data_Array is array (Index'Range) of Character;
   type RingBuffer is
      record
         Head : Index;
         Tail : Index;
         Data : Data_Array;
      end record;
end;
