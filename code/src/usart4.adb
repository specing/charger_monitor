--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with stm32f072.GPIO;
with stm32f072.RCC;
with stm32f072.USART;
with stm32f072.NVIC;

with Ring_Buffer;
with System.Machine_Code;

package body USART4 is

   NVIC : stm32f072.NVIC.NVIC_Peripheral renames stm32f072.NVIC.NVIC_Periph;
   GPIOC : stm32f072.GPIO.GPIO_Peripheral renames stm32f072.GPIO.GPIOC_Periph;
   U4 : stm32f072.USART.USART_Peripheral renames stm32f072.USART.USART4_Periph;
   U4RB : Ring_Buffer.RingBuffer;

   procedure Enable_Interrupts
     with Inline is
   begin
      System.Machine_Code.Asm("cpsie i" & ASCII.LF & ASCII.HT, Volatile => True);
   end;

   procedure Disable_Interrupts
     with Inline is
   begin
      System.Machine_Code.Asm("cpsid i" & ASCII.LF & ASCII.HT, Volatile => True);
   end;


   procedure Init is
   begin
      stm32f072.RCC.RCC_Periph.APB1ENR.USART4EN := 1;
      stm32f072.RCC.RCC_Periph.AHBENR.IOPCEN := 1;
      -- USART4 is alternate function 0 .. AF0
      --  GPIOC.AFRH.Arr(2) :=
      GPIOC.MODER.Arr(10 .. 11) := (others => 2#10#); -- alternate function mode
      GPIOC.OSPEEDR.Arr(10 .. 11) := (others => 2#11#); -- High speed
      --  DIV_Fraction   at 0 range 0 .. 3;
      --  DIV_Mantissa   at 0 range 4 .. 15;
      -- 16#1a1# for 115200 baud at 48 MHz = 2#0001_1010_0001#
      U4.BRR := (DIV_Fraction => 2#0001#, DIV_Mantissa => 2#0001_1010#, others => 0);

      NVIC.ISER := 2#0010_0000_0000_0000_0000_0000_0000_0000#;
      --U4.CR1.RXNEIE := 1;
      --U4.CR1 := (UE => 1, TE => 1, RE => 1, others => <>); -- +200 bytes WTF?
      U4.CR1.UE := 1;
      U4.CR1.TE := 1;
      --U4.CR1.RE := 1;

      Ring_Buffer.Initialize(U4RB);
      U4_Put_Line("Boot");
   end;



   procedure U4_Put(C: Character) is
      use all type Ring_Buffer.RingBuffer;
   begin
      -- If transmision is in progress then we just add another character
      -- to the FIFO else we have to add the character and trigger the usart interrupt
      Disable_Interrupts;
      if Is_Empty(U4RB) then
         U4.CR1.TXEIE := 1; -- will hopefully trigger an interrupt right
         -- when ints are enabled
      end if;
      Put(U4RB, C);
      Enable_Interrupts;
   end;

   procedure USART34_ISR
     with Export, Convention => C, External_Name => "___USART34_ISR";
--   pragma Attach_Handler(USART34_ISR, 17);
   procedure USART34_ISR is
      use all type Ring_Buffer.RingBuffer;
      use all type stm32f072.Bit;
   begin
      if U4.ISR.TXE = 1 then
         Disable_Interrupts;
         if Is_Empty(U4RB) then
            U4.CR1.TXEIE := 0;
         else
            U4.TDR.TDR := stm32f072.Uint9(Character'Pos(Get(U4RB)));
         end if;
         Enable_Interrupts;
      end if;
   end;



   procedure U4_Put(S : String) is
   begin
      for C of S loop
         U4_Put(C);
      end loop;
   end;
   procedure U4_New_Line is
   begin
      U4_Put(ASCII.CR);
      U4_Put(ASCII.LF);
   end;

   procedure U4_Put_Line(S : String) is
   begin
      U4_Put(S);
      U4_New_Line;
   end;

   procedure U4_Put16(Data : Integer'Base) is
      Temp : Integer'Base := Data;

      subtype Digit_Range is Integer'Base range 0 .. 15;
      --Convert : constant array(Digit_Range) of Character := "0123456789abcdef";
      Digit : Digit_Range; --NotSigned;

      S : String(1..8);
      I : Integer := 1;

   begin
      if Data < 0 then
         U4_Put('-');
      end if;
      -- and I <= 8 prevents a call to LCH. TODO: replace by proof that I <= 8
      while I <= 8 loop
         --            Digit := Temp mod 16;
         Digit := abs(Temp rem 16);
         --            S(I) := Convert(Digit);
         if Digit < 10 then
            S(I) := Character'Val(Character'Pos('0') + Digit);
         else
            Digit:= Digit - 10;
            S(I) := Character'Val(Character'Pos('A') + Digit);
         end if;
         Temp := Temp / 16;
         exit when Temp = 0;
         I := I + 1;
      end loop;

      -- Now unroll the string back
      while I > 0 loop
         U4_Put(S(I));
         I := I - 1;
      end loop;
   end;


   procedure U4_Put(Data : Integer'Base) is
      Temp : Integer'Base := Data;

      subtype Digit_Range is Integer'Base range 0 .. 15;
      --Convert : constant array(Digit_Range) of Character := "0123456789abcdef";
      Digit : Digit_Range; --NotSigned;

      S : String(1..10);
      I : Integer := 1;

   begin
      if Data < 0 then
         U4_Put('-');
      end if;
      -- and I <= 8 prevents a call to LCH. TODO: replace by proof that I <= 8
      while I <= S'Last loop
         --            Digit := Temp mod 16;
         Digit := abs(Temp rem 10);
         --            S(I) := Convert(Digit);
         S(I) := Character'Val(Character'Pos('0') + Digit);
         Temp := Temp / 10;
         exit when Temp = 0;
         I := I + 1;
      end loop;

      -- Now unroll the string back
      while I > 0 loop
         U4_Put(S(I));
         I := I - 1;
      end loop;
   end;



end USART4;
