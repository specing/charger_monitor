--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

package USART4 is

   procedure Init;
   procedure U4_Put(C: Character);
   procedure U4_Put(S : String);
   procedure U4_New_Line;
   procedure U4_Put_Line(S : String);
   procedure U4_Put16(Data : Integer'Base);
   procedure U4_Put(Data : Integer'Base);

end USART4;
