--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with stm32f072; use stm32f072;

package Analog_Input is

   subtype ADC_Channel_Select_Type is UInt19;
   ADC_CHANNEL_IN0  : constant ADC_Channel_Select_Type := 2**0;
   ADC_CHANNEL_IN1  : constant ADC_Channel_Select_Type := 2**1;
   ADC_CHANNEL_IN2  : constant ADC_Channel_Select_Type := 2**2;
   ADC_CHANNEL_IN3  : constant ADC_Channel_Select_Type := 2**3;
   ADC_CHANNEL_IN4  : constant ADC_Channel_Select_Type := 2**4;
   ADC_CHANNEL_IN5  : constant ADC_Channel_Select_Type := 2**5;
   ADC_CHANNEL_IN6  : constant ADC_Channel_Select_Type := 2**6;
   ADC_CHANNEL_IN7  : constant ADC_Channel_Select_Type := 2**7;
   ADC_CHANNEL_IN8  : constant ADC_Channel_Select_Type := 2**8;
   ADC_CHANNEL_IN9  : constant ADC_Channel_Select_Type := 2**9;
   ADC_CHANNEL_IN10 : constant ADC_Channel_Select_Type := 2**10;
   ADC_CHANNEL_IN11 : constant ADC_Channel_Select_Type := 2**11;
   ADC_CHANNEL_IN12 : constant ADC_Channel_Select_Type := 2**12;
   ADC_CHANNEL_IN13 : constant ADC_Channel_Select_Type := 2**13;
   ADC_CHANNEL_IN14 : constant ADC_Channel_Select_Type := 2**14;
   ADC_CHANNEL_IN15 : constant ADC_Channel_Select_Type := 2**15;

   ADC_CHANNEL_TEMP : constant ADC_Channel_Select_Type := 2**16;
   ADC_CHANNEL_VREF : constant ADC_Channel_Select_Type := 2**17;
   ADC_CHANNEL_VBAT : constant ADC_Channel_Select_Type := 2**18;

   ADC_CHANNEL_BATT : ADC_Channel_Select_Type renames ADC_CHANNEL_IN13; -- PC3


   type ADC_Array_Type is array (0 .. 2) of Integer;
   Samples_Array, Result_Array : ADC_Array_Type;


   procedure Init;

   procedure Start_Conversion;

end Analog_Input;
