--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

--with Ada.Interrupts.Names;

with stm32f072.GPIO;
with stm32f072.RCC;
with stm32f072.TIM;
with stm32f072.NVIC;

with Analog_Input;
with Charger_System;
--with USART4; use USART4;

pragma Warnings(Off);
with Interfaces.STM32; use Interfaces.STM32;
pragma Warnings(On);

package body Monitor is

   Relay0_Pin : constant := 1;
   Relay0_Periph : stm32f072.GPIO.GPIO_Peripheral renames stm32f072.GPIO.GPIOF_Periph;

   NVIC : stm32f072.NVIC.NVIC_Peripheral renames stm32f072.NVIC.NVIC_Periph;
   TIM15 : stm32f072.TIM.TIM15_Peripheral renames stm32f072.TIM.TIM15_Periph;

   GPIOA : stm32f072.GPIO.GPIO_Peripheral renames stm32f072.GPIO.GPIOA_Periph;
   GPIOC : stm32f072.GPIO.GPIO_Peripheral renames stm32f072.GPIO.GPIOC_Periph;

   -- TODO: battery part of ADC_Temp_ISR into New_Measurement adds 20 bytes to code
   -- TODO: time-keeping (RTC), charge in the early morning (4 AM?)
   -- DONE: record charging parameters: starting voltage, end voltage, time taken,
   --       voltage drop - done (uart).

   Open_Drain : constant stm32f072.GPIO.OTYPER_OT_Element := 1;
   Output : constant stm32f072.GPIO.MODER_Element := 2#01#;


   procedure Monitor_Init is
   begin
      --  stm32f072rb discovery has 4 leds of different colours connected on
      --  PC6(red), PC7(blue), PC8(orange), PC9(green)
      --  Initialization is as follows:
      --     Enable GPIOC clock in RCC
      stm32f072.RCC.RCC_Periph.AHBENR.IOPCEN := 1;
      --     Configure all led pins to function as high-speed push-pull GPOs
      GPIOC.MODER.Arr(6 .. 9)   := (others => 2#01#); -- gen purp out

--        Led_Rotation := 0;


      -- Enable portA peripheral clock
      stm32f072.RCC.RCC_Periph.AHBENR.IOPAEN := 1;
      -- Enable analog input on PC3
      GPIOC.MODER.Arr(3) := 2#11#; -- Analog? p.150
      -- general input on PC13
      GPIOA.MODER.Arr(13) := 2#10#;

      -- Enable portB peripheral clock
      stm32f072.RCC.RCC_Periph.AHBENR.IOPBEN := 1;
      stm32f072.RCC.RCC_Periph.AHBENR.IOPFEN := 1;
      -- Configure PF1 open drain output
      Relay0_Periph.MODER.Arr(Relay0_Pin) := Output;
      Relay0_Periph.OTYPER.OT.Arr(Relay0_Pin) := Open_Drain;

      Relay0_Periph.BSRR.BS.Arr(Relay0_Pin) := 1; -- turn off relay (de-activate nmos)

      Charger_System.Init;
      Analog_Input.Init;
   end Monitor_Init;


   procedure Timer15_init is
      use type stm32f072.UInt32;
      use type stm32f072.UInt16;
   begin
      stm32f072.RCC.RCC_Periph.APB2ENR.TIM15EN := 1;

      --  Fire once per second (48MHz clock)
      TIM15.PSC.PSC := 750 - 1;
      TIM15.ARR.ARR := 64_000 - 1;

      --  interrupts : none!
      --  TIM15.DIER := (UIE => 1, others => <>);
      --  NVIC.ISER := 2**20;

      TIM15.CR2.MMS := 2#010#; -- TRGO = update event

      TIM15.CR1.CEN := 1;
   end;


   procedure TIM15_ISR
     with Export, Convention => C, External_Name => "___TIM15_ISR";
   procedure TIM15_ISR is
   begin
      TIM15.SR := (UIF => 0, Reserved_3_4 => 0, Reserved_8_8 => 0,
                   Reserved_11_31 => 0, others => 0);
   end;


end Monitor;
