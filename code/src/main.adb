--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Last_Chance_Handler;
with Monitor;
with System.Machine_Code;
with Interfaces;
with USART4;
with Real_Time_Clock;

procedure Main is
   use System.Machine_Code;
begin
   USART4.Init;
   Monitor.Monitor_init;
   Monitor.timer15_init;
   Real_Time_Clock.Init;
   Asm("cpsie i" & ASCII.LF & ASCII.HT, Volatile => True);

   loop
--      Monitor.Monitor_task;
      Asm("wfi" & ASCII.LF & ASCII.HT, Volatile => True);
--        declare
--           -- Using Integer here results in there being a comparison ensuring
--           -- that it never becomes signed and calling LCH if this occurs.
--           delay_counter : Interfaces.Unsigned_32 := 1000000 with Volatile;
--           use all type Interfaces.Unsigned_32;
--        begin
--           while delay_counter > 0 loop
--              delay_counter := delay_counter - 1;
--           end loop;
--        end;
   end loop;
end Main;
