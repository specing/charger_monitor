--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with stm32f072.RTC;

package Real_Time_Clock is

   type Date_Time is private;


   -- Assumption: RCC is clocked/otherwise initialised
   function Is_Initialised return Boolean;


--   procedure Init (Current_Time : Date_Time);
   procedure Init;

   procedure Get_DateTime (Current_Time : out Date_Time);

   function Hour (Time : in Date_Time) return Integer;

   type Character_Put is not null access procedure (ch : in Character);
   procedure Put (cp : in Character_Put; dt : Date_Time);

private
   type Date_Time is
      record
         tr : stm32f072.RTC.TR_Register;
         dr : stm32f072.RTC.DR_Register;
      end record;
end Real_Time_Clock;
