--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with stm32f072.GPIO;

with USART4; use USART4;
with Real_Time_Clock;

package body Charger_System is
   use all type stm32f072.Bit;

   Relay0_Pin : constant := 1;
   Relay0_Periph : stm32f072.GPIO.GPIO_Peripheral renames
     stm32f072.GPIO.GPIOF_Periph;

   Charger_Presence0_Pin : constant := 13;
   Charger_Presence0_Periph : stm32f072.GPIO.GPIO_Peripheral renames
     stm32f072.GPIO.GPIOC_Periph;

   Led_Green_Pin : constant := 9;
   Led_Green_Periph : stm32f072.GPIO.GPIO_Peripheral renames
     stm32f072.GPIO.GPIOC_Periph;

   procedure Init is
   begin
      Charger_State := IDLE;
      old_U_Battery := 0;
      old_old_U_Battery := 0;
   end;

   -- Things to handle:
   --   - Record battery voltage delta (Delta_U_charging) when charging
   --     starts, so we know when to terminate
   --   - If the charger is prematurely disconnected, abort charging.
   --     This can be detected by the battery voltage falling down at
   --     least Delta_U_charging/2 ?
   --   - If the battery is prematurely disconnected, abort charging.
   --     This can be detected by the battery voltage shooting up to ~42V?
   procedure New_Measurement (U_adc : Integer) is

      Desired_Battery_End_Voltage : constant := 41_000; -- mV

      -- The battery has a range of 30-42V. The resistor divider lowers this
      -- to somewhere between 0 and 3V.
      R_low : constant := 6460; --3_300 + 3_300;
      R_high: constant := 100_400;
      --    U_ADC = U_Battery * R_Low / (R_low + R_high)
      -- => U_ADC * (R_low + R_high) = U_Battery * R_Low
      -- => U_ADC * (R_low + R_high) / R_Low = U_Battery

      -- UBattery : Int := u64(Data) * (R_low + R_high) / R_low;
      -- Better numerical stability:
      -- type u64 is mod 2**64;

      -- stop_charge_thresh_volt : constant := 40.5;
      -- stop_charge_thresh_float: constant := stop_charge_thresh_volt * R_Low / (R_Low + R_High);
      -- stop_charge_thresh_adc  : constant := stop_charge_thresh_float * 4095.0;

      -- For the chosen R_low/R_high: (R_low+R_high)/ R_Low = 143/10
      U_Battery_Raw : constant Integer := U_adc * (R_low+R_high) / R_Low;
      --U_Battery : constant Integer := U_adc * 143 / 10;

      -- Corrected using a linear model  y = kx+n
      -- measurements with multimeter (a bit more accurate, I hope):
      y1 : constant Integer := 17520;
      y2 : constant Integer := 36150;
      -- what this code outputed at the above:
      x1 : constant Integer := 17700;
      x2 : constant Integer := 36656;
      -- Slope steepness k = K_Upper/K_lower
      K_upper : constant Integer := y2 - y1;
      K_lower : constant Integer := x2 - x1;

      U_Battery : constant Integer := K_upper* (U_Battery_Raw - x2) / K_lower + y2;

      -- Take two steps back, in case an event happened too close to one of the measurements.
      delta_U : constant Integer := U_Battery - old_old_U_Battery;

      procedure Start_Charging is
      begin
         U4_Put(",chg_start");
         Charger_State := CHARGING1;
         Relay0_Periph.BSRR.BR.Arr(Relay0_Pin) := 1; -- turn on relay
         Led_Green_Periph.BSRR.BS.Arr(Led_Green_Pin) := 1; -- turn on LED
      end;

      procedure Stop_Charging is
      begin
         U4_Put(",chg_end");
         Relay0_Periph.BSRR.BS.Arr(Relay0_Pin) := 1; -- turn off relay
         Led_Green_Periph.BSRR.BR.Arr(Led_Green_Pin) := 1; -- turn off LED
         Charger_State := END_OF_CHARGE;
      end;

      use Real_Time_Clock;
      DT : Real_Time_Clock.Date_Time;
   begin
      U4_Put(",U_batt_raw=");
      U4_Put(U_Battery_Raw);
      U4_Put("mV,U_bat=");
      U4_Put(U_Battery);
      U4_Put("mV,chg_pres=");
      U4_Put(Integer(Charger_Presence0_Periph.IDR.IDR.Arr(Charger_Presence0_Pin)));
      Get_DateTime(DT);

      -- State machine
      case Charger_State is
         when TIME_WAIT =>
            U4_Put(",time_wait");
            if Hour(DT) > 22 or Hour(DT) < 6 then
               Charger_State := IDLE;
            end if;
         when IDLE =>
            U4_Put(",idle");
            if U_Battery > 25_000 and U_Battery < Desired_Battery_End_Voltage
              and Charger_Presence0_Periph.IDR.IDR.Arr(Charger_Presence0_Pin) = 1 then
               Start_Charging;
            else
               Charger_State := TIME_WAIT;
            end if;
         when CHARGING1 =>
            Charger_State := CHARGING2;
         when CHARGING2 =>
            Charger_State := CHARGING;
            Delta_U_Charging := delta_U;
            U4_Put (",Delta_U_Charging="); U4_Put (Delta_U_Charging); U4_Put ("mV");
         when CHARGING =>
            if Delta_U < -200 then -- -0.2V, charger disconnected?
               U4_Put (",chg_conn_break?");
               Stop_Charging;
            elsif Delta_U > 200 then -- Battery disconnected?
               U4_Put (",battery_conn_break?");
               Stop_Charging;
            elsif U_Battery > Desired_Battery_End_Voltage + Delta_U_Charging then
               U4_Put (",EoC_threshold_reached");
               Stop_Charging;
               -- else continue charging
            end if;
         when END_OF_CHARGE =>
            if U_Battery < 25_000 then -- disconnected
               Charger_State := TIME_WAIT;
            end if;
      end case;

      old_old_U_Battery := old_U_Battery;
      old_U_Battery := U_Battery;
   end;

end Charger_System;
