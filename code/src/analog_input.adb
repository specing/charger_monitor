--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Unchecked_Conversion;
with System;

with stm32f072.ADC;
with stm32f072.DMA;
with stm32f072.NVIC;
with stm32f072.RCC;

with Charger_System;
with Real_Time_Clock;
with USART4; use USART4;

package body Analog_Input is

   use all type stm32f072.Bit;

   ADC : stm32f072.ADC.ADC_Peripheral renames stm32f072.ADC.ADC_Periph;
   NVIC : stm32f072.NVIC.NVIC_Peripheral renames stm32f072.NVIC.NVIC_Periph;
   DMA1 : stm32f072.DMA.DMA1_Peripheral renames stm32f072.DMA.DMA1_Periph;

   type ADC_Capture_t is range 0 .. 2**12-1;

   TEMP110_CAL : aliased ADC_Capture_t'Base --stm32f072.Uint16
     with Import, Address => System'To_Address(16#1FFF_F7C2#);
   TEMP30_CAL : aliased stm32f072.Uint16
     with Import, Address => System'To_Address(16#1FFF_F7B8#);
   VREFINT_CAL : aliased stm32f072.Uint16
     with Import, Address => System'To_Address(16#1FFF_F7BA#);
   VDD_Calib : constant := 330;
   VDD_Appli : constant := 300;


   function Address_to_Uint32 is new Ada.Unchecked_Conversion
     (Source => System.Address, Target => UInt32);


   procedure Init is
   begin
      -- Enable peripheral clock (watch out, there are two clock sources)
      stm32f072.RCC.RCC_Periph.APB2ENR.ADCEN := 1;
      ADC.CFGR2.JITOFF_D.Val := 2#10#; -- PCLK/4 sync mode

      -----------------
      --  Calibrate ADC  (RM0091r9p937)
      --    ADEN will be clear at reset
--        if ADC.CR.ADEN = 1 then
--           ADC.CR.ADDIS := 1;
--        end if;
--        loop
--           exit when ADC.CR.ADEN = 0;
--        end loop;
      --    DMAEN will be clear at reset
--        ADC.CFGR1.DMAEN := 0;
      --
      ADC.CR.ADCAL := 1;
      loop
         exit when ADC.CR.ADCAL = 0;
         -- have to wait for 4 ADC clock cycles after ADCAL is set to 0?
         -- RM0091r9 page 232
      end loop;



      ----------------
      -- Turn ON ADC
      --   RM0091 page 231, 937  ADC turn on/off
      --   ADRDY should be 0 at reset
      if ADC.ISR.ADRDY = 1 then
         ADC.ISR.ADRDY := 1; -- clear it
      end if;

      loop
         ADC.CR.ADEN := 1; -- See f072rb errata for this being in the loop.
         exit when ADC.ISR.ADRDY = 1;
      end loop;

      -- Reading the temperature, RM0091r9p251
      ADC.CCR.TSEN := 1;
      ADC.CCR.VREFEN := 1;

      -- Enable interrupts
      --NVIC.ISER := 2#0000_0000_0000_0000_0001_0000_0000_0000#;
      --ADC.IER.EOCIE := 1; -- End Of Conversion

      -- highest sampling time (slowest), should be foolproof
      -- Datasheet says 4 micro seconds for f072rb
      ADC.SMPR.SMPR := 2#111#; -- 20us when using 48MHz=PCLK/4
      ADC.CFGR1.EXTSEL := 2#100#; -- TIM15 trigger
      ADC.CFGR1.EXTEN := 2#01#; -- rising edge trigger

      -- Wait for ADC data being read before starting new conversion.
      ADC.CFGR1.AUTDLY := 1; -- aka WAIT

      ADC.CHSELR.CHSEL.Val := ADC_CHANNEL_VREF
        or ADC_CHANNEL_TEMP or ADC_CHANNEL_BATT;


      stm32f072.RCC.RCC_Periph.AHBENR.DMA1EN := 1;
      ADC.CFGR1.DMAEN := 1;
      ADC.CFGR1.DMACFG := 1; -- ADC DMA circular mode

      DMA1.CPAR1 := Address_to_Uint32(ADC.DR'Address);
      DMA1.CMAR1 := Address_to_Uint32(Samples_Array'Address);
      DMA1.CNDTR1.NDT := 3;
      DMA1.CCR1 := (MEM2MEM => 0, -- peripheral<->memory
                    PL => 0, -- low priority
                    MSIZE => 2, PSIZE => 1, -- 32bit<->16bit
                    MINC => 1, PINC => 0,
                    CIRC => 1, DIR => 0, -- circular mode, periph->mem dir
                    TEIE => 1, HTIE => 0, TCIE => 1, -- complete and error ints
                    EN => 1,
                    Reserved_15_31 => 0
                   );

      NVIC.ISER := 2**9; -- DMA1 CH1 interrupt enable


      ADC.CR.ADSTART := 1; -- Starts at next hardware event.


      U4_Put("T30=");
      U4_Put(Integer(TEMP30_CAL));
      U4_Put(",T110=");
      U4_Put(Integer(TEMP110_Cal));
      U4_Put(",VREFINT_CAL=");
      U4_put(Integer(VREFINT_CAL));
      U4_New_Line;
   end;


   procedure Start_Conversion is
   begin
      ADC.CR.ADSTART := 1;
   end;


   procedure Process_Samples is
      Temperature : Integer renames Result_Array(1);
      U_vcc : Integer renames Result_Array(2);

      dt : Real_Time_Clock.Date_Time;
      i : Integer := Samples_Array'First;
   begin
      Real_Time_Clock.Get_DateTime(dt);
      Real_Time_Clock.Put(U4_Put'Access, dt);

      -- print samples
      U4_put(",ADC=");
      loop
         U4_Put(Samples_Array(i));
         exit when i = Samples_Array'Last;
         i := i + 1;
         U4_Put(',');
      end loop;

      -- Start with VREF
      -- voltage unit is mili volt
      U_vcc := Integer(VREFINT_CAL) * 3300 / Samples_Array(2);
      -- What are reasonable bounds on U_ref?
      -- As stated by STM in datasheet page 55, it is designed to be around 1.2 - 1.25V
      -- The maximum operating voltage is 3.6V, therefore the minimum measured value will be ~2**12/3 > 1300 > 1024
      -- The minimum operating voltage is ~2V?, therefore the largest measured value will be 1.2/2 * 2**12 = 2457 < 2500
      -- What are reasonable bounds on VREFINT_CAL? If the voltage varies 3.2-3.4V, then the value will
      -- vary around 1400-1600.
      -- => log2(1600  * 3300 / 1024) = 12.3 < 13 bits long
      U4_Put(",Vcc=");
      U4_Put(U_vcc);
      U4_Put("mV");


      -- TODO: Octave:
      -- VREFINT_CAL = 0x605, VREFINT_DATA= 0x69f,
      -- TEMP30_CAL = 0x6e0, TEMP110_CAL = 0x526,

      -- Vdda = 3300 * VREFINT_CAL / VREFINT_DATA,
      -- TEMP30_CAL_mV = TEMP30_CAL * 3300 / 4095,
      -- TEMP110_CAL_mV = TEMP110_CAL * 3300 / 4095,
      -- temp_ADC = 0x7a3,
      -- temp_mV = Vdda * temp_ADC  / 4095,
      -- k = (110-30) / (TEMP110_CAL_mV - TEMP30_CAL_mV),
      -- x = temp_mV - TEMP30_CAL_mV,
      -- Tj = k*x + 30
      -- Tj = ((110-30) / (TEMP110_CAL_mV - TEMP30_CAL_mV))*x + 30
      Temperature := Samples_Array(1) * VDD_Appli;
      -- Vdda : constant Int := (3300 * Int(VREFINT_CAL)) / Int(Vref_int);
      -- Temp_mV : Int := (Vdda * Int(Data)) / 4095;
      -- x : Int := temp_mV - TEMP30_CAL_mV;
      -- k : Int := (110-30) / (temp
      Temperature := Temperature / VDD_Calib;
      Temperature := Temperature - Integer(TEMP30_CAL);
      -- Temperature
      Temperature := Temperature * (110 - 30);
      Temperature := Temperature / (Integer(TEMP110_CAL) - Integer(TEMP30_CAL));
      Temperature := Temperature + 30;

      U4_Put(",Tj=");
      U4_Put(Integer(Temperature));
      U4_Put("'C");

      -- However, U_ADC is not in volts or milivolts, it is expressed as a fraction of U_VCC
      -- More precisely, as U_ADC/4096, thus one ADC step is U_ADC/4096 volts

      -- Correct reading
      -- 13+12 bits / 12 bits => 13 bits

      Result_Array(0) := Samples_Array(0) * U_vcc / 4095;
      U4_Put(",U_pin=");
      U4_Put(Result_Array(0));
      U4_Put("mV");

      Charger_System.New_Measurement (Result_Array(0));

      U4_New_Line;
   end;



   procedure DMA_CH1_ISR
     with Export, Convention => C, External_Name => "___DMA_CH1_ISR";

   procedure DMA_CH1_ISR is
   begin
      if DMA1.ISR.TCIF1 = 1 then
         DMA1.IFCR.CTCIF1 := 1;
         Process_Samples;
      elsif DMA1.ISR.TEIF1 = 1 then
         U4_Put_Line("Error on DMA1CH1");
         DMA1.IFCR.CTEIF1 := 1;
      end if;
   end;


   procedure ADC_COMP_ISR
     with Export, Convention => C, External_Name => "___ADC_COMP_ISR";

   procedure ADC_COMP_ISR is
--      Data : ADC_Capture_t'Base;
   begin
      U4_Put_Line("Error: Unexpected entry into ADC ISR.");
--      if ADC.ISR.EOC = 1 then
--         Data := ADC_Capture_t'Base(ADC.DR.DATA);
--      end if;
   end ADC_COMP_ISR;

end Analog_Input;
