--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

package body Ring_Buffer is
   procedure Initialize(RB: in out RingBuffer) is
   begin
      RB.Head := 0;
      RB.Tail := 0;
   end;

   procedure Put(RB: in out RingBuffer; C : in Character) is
   begin
      if not Is_Full(RB) then
         RB.Data(RB.Tail) := C;
         RB.Tail := RB.Tail + 1;
         --else we cry
      end if;
   end;

   function Get(RB: in out RingBuffer) return Character is
   begin
      return R : Character do
         R := RB.Data(RB.Head);
         RB.Head := RB.Head + 1;
      end return;
   end;

   function Is_Empty(RB: in RingBuffer) return Boolean is
     (RB.Head = RB.Tail);

   function Is_Full(RB: in RingBuffer) return Boolean is
     (RB.Tail = RB.Head - 1);
end;
