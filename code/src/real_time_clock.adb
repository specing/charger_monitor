--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with stm32f072.PWR;
with stm32f072.RCC;

package body Real_Time_Clock is

   use type stm32f072.Bit;

   periph: stm32f072.RTC.RTC_Peripheral renames stm32f072.RTC.RTC_Periph;
   PWR : stm32f072.PWR.PWR_Peripheral renames stm32f072.PWR.PWR_Periph;
   RCC : stm32f072.RCC.RCC_Peripheral renames stm32f072.RCC.RCC_Periph;

   function Is_Initialised return Boolean is
   begin
      return RCC.BDCR.RTCEN = 1 and then periph.ISR.INITS = 1;
   end;


   type RTC_Date_Format is (fmt_24hour, fmt_AMPM);
   for RTC_Date_Format use (fmt_24hour => 0,
                            fmt_AMPM => 1);


   --   procedure Init (Current_Time : Date_Time) is
   procedure Init is
      -- Default 2000.01.01 (Saturday) at 00:00:00
      d_reg : constant stm32f072.RTC.DR_Register := (DT => 0, DU => 1,
                                                     MT => 0, MU => 1,
                                                     WDU => 2#110#,
                                                     YT => 0, YU => 0,
                                                     others => <>);

      t_reg : constant stm32f072.RTC.TR_Register := (HT => 0, HU => 0,
                                                     MNT => 0, MNU => 0,
                                                     ST => 0, SU => 0,
                                                     others => <>);
   begin
      if Is_Initialised then
         return;
      end if;

      --  enable RTC domain write access
      RCC.APB1ENR.PWREN := 1;
      PWR.CR.DBP := 1;

      RCC.CSR.LSION := 1;
      loop
         exit when RCC.CSR.LSIRDY = 1;
      end loop;

      RCC.BDCR.RTCSEL := 2#10#; -- LSI
      RCC.BDCR.RTCEN  := 1;


      -- unlock registers
      periph.WPR.KEY := 16#ca#;
      periph.WPR.KEY := 16#53#;

      periph.ISR.INIT := 1;
      loop
         exit when periph.ISR.INITF = 1;
      end loop;

      -- for 40 kHz LSI
      periph.PRER.PREDIV_A := 16#7f#;
      periph.PRER.PREDIV_S := 312;

--      periph.CR.FMT := Bit(fmt_24hour);
      periph.CR.BYPSHAD := 1;

      periph.DR := d_reg;
      periph.TR := t_reg;

      periph.ISR.INIT := 0;
   end;



   procedure Get_DateTime (Current_Time : out Date_Time) is
      use type stm32f072.RTC.TR_Register;
   begin
      loop
         Current_Time.tr := periph.TR;
         Current_Time.dr := periph.DR;
         exit when Current_Time.tr = periph.TR;
      end loop;
   end;

   function Hour (Time : in Date_Time) return Integer is
   begin
      return Integer(Time.tr.HT) * 10 + Integer(Time.tr.HU);
   end;



   -- TODO: generics or something
   procedure Put (cp : in Character_Put; dt : Date_Time) is
      procedure Output_Digit(digit: in Integer) is
      begin
         cp (Character'Val(digit + 16#30#));
      end;
      procedure Output_Digit(digit: in stm32f072.Bit) is
      begin
         Output_Digit(Integer(digit));
      end;

      procedure Output_Digit(digit: in stm32f072.UInt2) is
      begin
         Output_Digit(Integer(digit));
      end;
      procedure Output_Digit(digit: in stm32f072.UInt3) is
      begin
         Output_Digit(Integer(digit));
      end;
      procedure Output_Digit(digit: in stm32f072.UInt4) is
      begin
         Output_Digit(Integer(digit));
      end;

   begin -- YYYY.MM.DD_HH:MM:SS format
      cp ('2'); cp('0'); Output_Digit (dt.dr.YT); Output_Digit(dt.dr.YU);
      cp ('.'); Output_Digit (dt.dr.MT); Output_Digit(dt.dr.MU);
      cp ('.'); Output_Digit (dt.dr.DT); Output_Digit(dt.dr.DU);
      cp ('_'); Output_Digit (dt.tr.ht); Output_Digit(dt.tr.hu);
      cp (':'); Output_Digit (dt.tr.mnt);Output_Digit(dt.tr.mnu);
      cp (':'); Output_Digit (dt.tr.st); Output_Digit(dt.tr.su);
   end;

end Real_Time_Clock;
