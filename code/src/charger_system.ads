--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

package Charger_System is
   procedure Init;
   procedure New_Measurement (U_adc : Integer);
private
   old_U_Battery, old_old_U_Battery: Integer;
   Delta_U_Charging : Integer;

   -- the CHARGING1 and CHARGING2 states are to record Delta_U_Charging
   type Charger_State_t is (TIME_WAIT, IDLE, CHARGING1, CHARGING2, CHARGING, END_OF_CHARGE);
   Charger_State : Charger_State_t;

end Charger_System;
