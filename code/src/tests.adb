--  Charger monitor, a stm32-based control manager for non-ideal chargers.
--  Copyright (C) 2019  Fedja Beader
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ring_Buffer;
with Ada.Text_IO; use Ada.Text_IO;


procedure Tests is
   use all type Ring_Buffer.RingBuffer;

   RB : Ring_Buffer.RingBuffer;

   Str : constant String := "Hello world";


   procedure Enable_Interrupts is null;
   procedure Disable_Interrupts is null;


   procedure U4_Put(C: Character) is
      use all type Ring_Buffer.RingBuffer;
   begin
      -- If transmision is in progress then we just add another character
      -- to the FIFO else we have to add the character and trigger the usart interrupt
      Disable_Interrupts;
      Put(C);
      Enable_Interrupts;
   end;


   procedure U4_Put(S : String) is
   begin
      for C of S loop
         U4_Put(C);
      end loop;
   end;
   procedure U4_New_Line is
   begin
      U4_Put(ASCII.CR);
      U4_Put(ASCII.LF);
   end;

   procedure U4_Put_Line(S : String) is
   begin
      U4_Put(S);
      U4_New_Line;
   end;

   procedure U4_Put(Data : Integer'Base) is
      Temp : Integer'Base := Data;

      subtype Digit_Range is Integer'Base range 0 .. 15;
      --Convert : constant array(Digit_Range) of Character := "0123456789abcdef";
      Digit : Digit_Range; --NotSigned;

      S : String(1..10);
      I : Integer := 1;

   begin
      if Data < 0 then
         U4_Put('-');
      end if;
      -- and I <= 8 prevents a call to LCH. TODO: replace by proof that I <= 8
      while I <= S'Last loop
         --            Digit := Temp mod 16;
         Digit := abs(Temp rem 10);
         --            S(I) := Convert(Digit);
         S(I) := Character'Val(Character'Pos('0') + Digit);
         Temp := Temp / 10;
         exit when Temp = 0;
         I := I + 1;
      end loop;

--      Put_Line("Intermediate string: " & S);
      -- Now unroll the string back
      while I > 0 loop
--         Put("i:" & Integer'Image(I) & ", ");
         U4_Put(S(I));
         I := I - 1;
      end loop;
   end;



begin
   Initialize(RB);
   for i in Str'Range loop
      if Is_Full(RB) then
         Put_Line("RB is full!");
      else
         Put(RB, Str(I));
      end if;
   end loop;

   Put("Contents of RB: ");

   while not Is_Empty(RB) loop
      Put(Get(RB));
      exit;
   end loop;

   Put(RB, '.');

   while not Is_Empty(RB) loop
      Put(Get(RB));
   end loop;
   New_Line;


   declare
      type Data_Type is mod 2**12;
      Data : Data_Type := 100;
      T : Data_Type;

      S : String(1..10);
      I : Integer := 1;
   begin
      while Data > 0 loop
         T := Data mod 16;
         if T < 10 then
            S(I) := Character'Val(Character'Pos('0') + T);
         else
            T := T - 10;
            S(I) := Character'Val(Character'Pos('A') + T);
         end if;
         I := I + 1;
         Data := Data / 16;
      end loop;
      -- Now unroll the string back
      for Ind in reverse 1 .. I-1 loop
         Put(S(Ind));
      end loop;

      New_Line;
   end;

   U4_Put("min: "); U4_Put(Integer'First);
   U4_Put("zero: "); U4_Put(0);
   U4_Put("five: "); U4_Put(5);
   U4_Put("1910: "); U4_Put(1910);
   U4_Put("max: "); U4_Put(Integer'Last);

end;
