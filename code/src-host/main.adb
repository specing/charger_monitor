with Ada.Streams;
with Ada.Text_IO; use Ada.Text_IO;

with GNAT.Serial_Communications; use GNAT.Serial_Communications;

with Serial_Port; use Serial_Port;

procedure Main is
   ch : Character;
begin
   Put_Line("Starting.. to exit, press q");

   Serial_Task.Start;

   loop
      Get(ch);

      case ch is
      when 'q' =>
         Put_Line("Exiting...");
         if not Serial_Task'Terminated then
            abort Serial_Task;
         end if;
         exit;
      when others =>
            null;
      end case;
   end loop;


   --     SP.Read(Buffer => Buffer,
   --             Last   => Last);

   --   for i in 1 .. Last loop


end Main;
