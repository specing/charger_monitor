package Serial_Port is

   type Writer_Callback is not null access procedure(msg : in String);



   task Serial_Task is
      entry Start;
   end Serial_Task;

end Serial_Port;
