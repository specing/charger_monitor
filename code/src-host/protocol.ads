with Ada.Strings.Unbounded;

package Protocol is

   procedure Line_From_Mcu(reply : in out Ada.Strings.Unbounded.Unbounded_String;
                           line : String);

end Protocol;
