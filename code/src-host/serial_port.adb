with Ada.Streams;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

with GNAT.Serial_Communications; use GNAT.Serial_Communications;

with Protocol;

package body Serial_Port is

   task body Serial_Task is
      SP : aliased GNAT.Serial_Communications.Serial_Port;

      Buffer : Ada.Streams.Stream_Element_Array(1..100);
      Last   : Ada.Streams.Stream_Element_Offset := Buffer'First;

      str : Unbounded_String;
      ch : Character;

      procedure Send_Over_Link(msg : String) is
      begin
         String'Write(SP'Access, msg);
      end;

      reply : Unbounded_String;
   begin
      accept Start;

      SP.Open("/dev/ttyUSB0");

      SP.Set(Rate      => B115200,
             Bits      => CS8,
             Stop_Bits => One,
             Parity    => None,
             Block     => True,
             Local     => True,
             Flow      => None,
             Timeout   => 10.0);

      loop
         Character'Read (SP'Access, ch);

         case ch is
            when ASCII.CR =>
               null; -- ignore
            when ASCII.LF =>
               reply := Null_Unbounded_String;
               Protocol.Line_From_Mcu (reply, To_String(str));
               Send_Over_Link(To_String(reply));
               Delete(str, 1, length(str));
            when others =>
               Append(str, ch);
         end case;
      end loop;


      SP.Close;
   end;

end Serial_Port;
