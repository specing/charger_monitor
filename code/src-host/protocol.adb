with Ada.Calendar.Formatting;
with Ada.Calendar.Time_Zones;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Fixed;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

package body Protocol is

   package AC  renames Ada.Calendar;
   package ACF renames Ada.Calendar.Formatting;

   type State_Type is (pref_r, pref_e, pref_p, pref_dash,
                       year_m, year_h, year_t, year_u,
                       month_t, month_u, day_t, day_u,
                       sep, hour_t, hour_u, minute_t, minute_u, second_t, second_u
                      );
   State : State_Type := pref_r;

   procedure process_reply (reply : Character) is

      type DR_Register is
         record
            DU, DT, MU, MT, WDU, YU, YT : Integer;
         end record;

      type TR_Register is
         record
            SU, ST, MNU, MNT, HU, HT : Integer;
         end record;
      DR : DR_Register;
      TR : TR_Register;
   begin
      -- Current state is what is expected, i.e. in state pref_r we expect
      -- r to be given on input
      case State is
         when pref_r =>
            if reply = 'r' then
               State := pref_e; return;
            end if;
         when pref_e =>
            if reply = 'e' then
               State := pref_p; return;
            end if;
         when pref_p =>
            if reply = 'p' then
               State := pref_dash; return;
            end if;
         when pref_dash =>
            if reply = '-' then
               State := year_m; return;
            end if;

         when year_m =>
            if reply in '0' .. '9' then
               State := year_h; return;
            end if;
         when year_h =>
            if reply in '0' .. '9' then
               State := year_t; return;
            end if;
         when year_t =>
            if reply in '0' .. '9' then
               DR.YT := Character'Pos(reply) - Character'Pos('0');
               State := year_u; return;
            end if;
         when year_u =>
            if reply in '0' .. '9' then
               DR.YU := Character'Pos(reply) - Character'Pos('0');
               State := month_t; return;
            end if;

         when month_t =>
            if reply in '0' .. '9' then
               DR.MT := Character'Pos(reply) - Character'Pos('0');
               State := month_u; return;
            end if;
         when month_u =>
            if reply in '0' .. '9' then
               DR.MU := Character'Pos(reply) - Character'Pos('0');
               State := day_t; return;
            end if;

         when day_t =>
            if reply in '0' .. '9' then
               DR.DT := Character'Pos(reply) - Character'Pos('0');
               State := day_u; return;
            end if;
         when day_u =>
            if reply in '0' .. '9' then
               DR.DU := Character'Pos(reply) - Character'Pos('0');
               State := sep; return;
            end if;

         when sep =>
            if reply = '_' then
               State := hour_t; return;
            end if;
         when hour_t =>
            if reply in '0' .. '9' then
               TR.HT := Character'Pos(reply) - Character'Pos('0');
               State := hour_u; return;
            end if;
         when hour_u =>
            if reply in '0' .. '9' then
               TR.HU := Character'Pos(reply) - Character'Pos('0');
               State := minute_t; return;
            end if;

         when minute_t =>
            if reply in '0' .. '9' then
               TR.MNT := Character'Pos(reply) - Character'Pos('0');
               State := minute_u; return;
            end if;
         when minute_u =>
            if reply in '0' .. '9' then
               TR.MNU := Character'Pos(reply) - Character'Pos('0');
               State := second_t; return;
            end if;

         when second_t =>
            if reply in '0' .. '9' then
               TR.ST := Character'Pos(reply) - Character'Pos('0');
               State := second_u; return;
            end if;
         when second_u =>
            if reply in '0' .. '9' then
               TR.SU := Character'Pos(reply) - Character'Pos('0');

               New_Line;
               Put("D:"); Put(DR.YT); Put(DR.YU); Put(DR.MT); Put(DR.MU); Put(DR.DT); Put(DR.DU);
               Put("T: "); Put(TR.HT); Put(TR.HU); Put(TR.MNT); Put(TR.MNU); Put(TR.ST); Put(TR.SU);
               New_Line;
            end if;
      end case;

      State := State_Type'First; -- reset in case of invalid input
   end;

   procedure process_reply (reply : String) is
   begin
      for ch of reply loop
         process_reply(ch);
      end loop;
   end;


   -- YYYYMMDD_HHMMSS
   function Time_Stamp(Now : AC.Time) return String is
      function Trim (inp : String) return String is
         -- 'Image returns something prefixed with a space
         trimmed : constant String := inp(inp'First+1 .. inp'Last);

      begin
         if trimmed'Length < 2 then
            return "0" & trimmed;
         else
            return trimmed;
         end if;
      end;
   begin
      return Trim(AC.Year_Number'Image(ACF.Year(Now)))
        & Trim(AC.Month_Number'Image(ACF.Month(Now)))
        & Trim(AC.Day_Number'Image(ACF.Day(Now)))
        & "_"
        & Trim(ACF.Hour_Number'Image(ACF.Hour(Now)))
        & Trim(ACF.Minute_Number'Image(ACF.Minute(Now)))
        & Trim(ACF.Second_Number'Image(ACF.Second(Now)));
   end;


   procedure Time_Request (reply : in out Ada.Strings.Unbounded.Unbounded_String) is
      Now : Ada.Calendar.Time := Ada.Calendar.Clock;
      -- YYYYMMDD_HHMMSS.nanosec format?
   begin
      Append(reply, "rep-");
      Append(reply, Time_Stamp(Now));

      Put_Line ("Current ts in reply to mcu: " & To_String(reply));
      process_reply(To_String(reply));
   end;


   Log_File : Ada.Text_IO.File_Type;
   -- save for 2 more minutes past end of charge
   Save_Timeout : constant Natural := 120;
   type Saving_State_t is (Saving, In_Timeout, Not_Saving);
   Saving_State : Saving_State_t := Saving;
   Saving_For : Natural := 0;


   procedure Save_Line (line : String) is
      procedure Reopen_Log is
         Now : constant Ada.Calendar.Time := AC.Clock;
         File_Name : constant String := "log_" & Time_Stamp(Now);
      begin
         Put_Line("Opening log file: " & File_Name);

         if Is_Open (Log_File) then
            Close (Log_File);
         end if;

         Ada.Text_IO.Create (Log_File, Out_File, File_Name);
      exception
         when others =>
            Put("!!! Exception while attempting to create logfile.");
      end;

   begin
      if line = "Boot" or not Is_Open (Log_File) then
         -- Open a new log file with current time stamp
         Reopen_Log;

         Saving_State := In_Timeout;
         Saving_For := 10; -- save 10 lines after boot
      elsif 0 /= Ada.Strings.Fixed.Index(line, "chg_start") then
         Saving_State := Saving;
         Put_Line("Started saving");
      elsif 0 /= Ada.Strings.Fixed.Index(line, "chg_end") then
         Saving_State := In_Timeout;
         Saving_For := Save_Timeout;
      end if;

      if Saving_State /= Not_Saving then
         Put_Line (Log_File, line);
         if Saving_State = In_Timeout then
            if Saving_For > 0 then
               Saving_For := Saving_For - 1;
            else
               Saving_State := Not_Saving;
               Put_Line("Stopped saving");
               Flush (Log_File);
            end if;
         end if;
      end if;
   end;


   procedure Line_From_Mcu(reply : in out Ada.Strings.Unbounded.Unbounded_String;
                           line : String) is
   begin
      Put_Line("Received: " & line);
      Save_Line (line);
      Time_Request (reply);

      if line'Length >= 5 and line(line'first..line'first+3) = "req-" then
         if line (line'first+4 .. line'Last) = "time" then
            Time_Request (reply);
         end if;
      end if;
   end;


end Protocol;
